<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-deep-purple.css">
    <title>Tes Skala DASS-42</title>
    <style>
        .wa img {
            position: fixed;
            bottom: 6%;
            left: 5%;
            opacity: 0.85;
            max-width: 70px;
            height: auto;
        }

        .wa p {
            position: fixed;
            bottom: 2%;
            left: 3%;
        }
    </style>

  <title>Prediksi Naive Bayes V.1</title>
</head>
<body>

  <!-- <nav class="navbar navbar-expand-lg fixed-top navbar-light bg-light static-top">
    <div class="container">
      <a class="navbar-brand" href="index.php">
            <img src="img/nbc.png" alt="" width=50 height=50>
          </a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item active">
            <a class="nav-link" href="index.php">Prediksi<span class="sr-only">(current)</span> </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="data_simulasi.php">Data</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="about.php">Informasi</a>
          </li>
        </ul>
      </div>
    </div>
</nav> -->

     <div class="container" style='margin-top:90px'>
     <!--  <div class="row">
        <div class="col-12 mt-5">
          <h2 class="tebal">Metode Naive Bayes</h2>
          <p class="desc mt-4">Naïve Bayes Classifier merupakan sebuah metoda klasifikasi yang berakar pada teorema Bayes.
          Metode pengklasifikasian dengan menggunakan metode probabilitas dan statistik yg dikemukakan oleh ilmuwan Inggris Thomas Bayes, yaitu memprediksi peluang di masa depan berdasarkan pengalaman di masa sebelumnya sehingga dikenal sebagai Teorema Bayes.<br/><br/>
          Ciri utama dr Naïve Bayes Classifier ini adalah asumsi yang sangat kuat (naïf) akan independensi dari masing-masing kondisi / kejadian.<br/><br/>
          Menurut Olson Delen (2008) menjelaskan Naïve Bayes untuk setiap kelas keputusan, menghitung probabilitas dg syarat bahwa kelas keputusan adalah benar, mengingat vektor informasi obyek. Algoritma ini mengasumsikan bahwa atribut obyek adalah independen.<br/><br/>
          Probabilitas yang terlibat dalam memproduksi perkiraan akhir dihitung sebagai jumlah frekuensi dari " master " tabel keputusan.</p>
        </div> 
      </div> -->

    <div class="row">
      <div class="col-12 mt-4">
        <h3 class="tebal">Sistem Pakar Deteksi Dini Kesehatan Mental Menggunakan<br> Metode Naive Bayes</h3>
      </div> <br>

<!-- dribbble -->
<a class="credit" href="https://dribbble.com/shots/4647673-Material-Design-Radio-Buttons" target="_blank"><img src="https://cdn.dribbble.com/assets/logo-footer-hd-a05db77841b4b27c0bf23ec1378e97c988190dfe7d26e32e1faea7269f9e001b.png" alt=""></a>

          <section class="w3-container w3-section w3-theme-l2">
                <h4>Petunjuk Test :</h4>
                <ol>
                    <li>Jawablah semua pertanyaan sesuai dengan kondisi yang dialami dalam 1 bulan trakhir ini.</li>
                    <li>Setiap jawaban yang dijawab akan mendapatkan skor.</li>
                    <li>Jawaban yang sesuai kondisi, maka akan semakin akurat hasilnya.</li>
                    <li>Pastikan semua pertanyaan sudah terjawab, dan jika sudah semua terjawab baru kemudian klik Cek Hasil Test untuk memperoleh hasil test.</li>
                    <li>Selamat Mengerjakan!</li>
                </ol>
            </section>

            <section class="w3-responsive">
                <table class="w3-table w3-bordered w3-border">
                    <tr class="w3-theme">
                        <th>No</th>
                        <th style="width: 60%;" class="w3-center">Pertanyaan</th>
                        <th colspan="4" class="w3-center">Jawaban</th>
                    </tr>

                    <tr>
                        <td rowspan="2">1</td>
                        <td rowspan="2" style="width: 60%;">Saya merasa hampir panik. </td>
                        <td class="w3-center">
                            Tidak Ada
                        </td>
                        <td class="w3-center">
                            Kadang-kadang
                        </td>
                        <td class="w3-center">
                            Sering
                        </td>
                        <td class="w3-center">
                            Setiap saat
                        </td>
                    </tr>

                    <tr>
                        <td class="w3-center w3-pale-green">
                            <input type='radio' name='jawaban1' value='0' required="required">
                        </td>
                        <td class="w3-center w3-pale-green">
                            <input type='radio' name='jawaban1' value='1' required="required">
                        </td>
                        <td class="w3-center w3-pale-green">
                            <input type='radio' name='jawaban1' value='2' required="required">
                        </td>
                        <td class="w3-center w3-pale-green">
                            <input type='radio' name='jawaban1' value='3' required="required">
                        </td>
                    </tr>

                    <tr>
                        <td rowspan="2">2</td>
                        <td rowspan="2" style="width: 60%;">Saya merasa sulit untuk meningkatkan inisiatif untuk melakukan sesuatu.</td>
                        <td class="w3-center">
                            Tidak Ada
                        </td>
                        <td class="w3-center">
                            Kadang-kadang
                        </td>
                        <td class="w3-center">
                            Sering
                        </td>
                        <td class="w3-center">
                            Setiap saat
                        </td>
                    </tr>

                    <tr>
                        <td class="w3-center w3-pale-green">
                            <input type='radio' name='jawaban2' value='0' required="required">
                        </td>
                        <td class="w3-center w3-pale-green">
                            <input type='radio' name='jawaban2' value='1' required="required">
                        </td>
                        <td class="w3-center w3-pale-green">
                            <input type='radio' name='jawaban2' value='2' required="required">
                        </td>
                        <td class="w3-center w3-pale-green">
                            <input type='radio' name='jawaban2' value='3' required="required">
                        </td>
                    </tr>

                    <tr>
                        <td rowspan="2">3</td>
                        <td rowspan="2" style="width: 60%;">Saya merasa sedih dan sedih</td>
                        <td class="w3-center">
                            Tidak Ada
                        </td>
                        <td class="w3-center">
                            Kadang-kadang
                        </td>
                        <td class="w3-center">
                            Sering
                        </td>
                        <td class="w3-center">
                            Setiap saat
                        </td>
                    </tr>

                    <tr>
                        <td class="w3-center w3-pale-green">
                            <input type='radio' name='jawaban3' value='0' required="required">
                        </td>
                        <td class="w3-center w3-pale-green">
                            <input type='radio' name='jawaban3' value='1' required="required">
                        </td>
                        <td class="w3-center w3-pale-green">
                            <input type='radio' name='jawaban3' value='2' required="required">
                        <td class="w3-center w3-pale-green">
                            <input type='radio' name='jawaban3' value='3' required="required">
                        </td>
                        </td>
                    </tr>

                    <tr>
                        <td rowspan="2">4</td>
                        <td rowspan="2" style="width: 60%;">Saya tidak toleran terhadap apa pun yang menghalangi saya untuk melanjutkan apa yang saya lakukan</td>
                        <td class="w3-center">
                            Tidak Ada
                        </td>
                        <td class="w3-center">
                            Kadang-kadang
                        </td>
                        <td class="w3-center">
                            Sering
                        </td>
                        <td class="w3-center">
                            Setiap saat
                        </td>
                    </tr>

                    <tr>
                        <td class="w3-center w3-pale-green">
                            <input type='radio' name='jawaban4' value='0' required="required">
                        </td>
                        <td class="w3-center w3-pale-green">
                            <input type='radio' name='jawaban4' value='1' required="required">
                        </td>
                        <td class="w3-center w3-pale-green">
                            <input type='radio' name='jawaban4' value='2' required="required">
                        </td>
                        <td class="w3-center w3-pale-green">
                            <input type='radio' name='jawaban4' value='3' required="required">
                        </td>
                    </tr>

                    <tr>
                        <td rowspan="2">5</td>
                        <td rowspan="2" style="width: 60%;">Saya merasa bahwa saya tidak punya apa-apa untuk dinanti-nantikan</td>
                        <td class="w3-center">
                            Tidak Ada
                        </td>
                        <td class="w3-center">
                            Kadang-kadang
                        </td>
                        <td class="w3-center">
                            Sering
                        </td>
                        <td class="w3-center">
                            Setiap saat
                        </td>
                    </tr>

                    <tr>
                        <td class="w3-center w3-pale-green">
                            <input type='radio' name='jawaban5' value='0' required="required">
                        </td>
                        <td class="w3-center w3-pale-green">
                            <input type='radio' name='jawaban5' value='1' required="required">
                        </td>
                        <td class="w3-center w3-pale-green">
                            <input type='radio' name='jawaban5' value='2' required="required">
                        </td>
                        <td class="w3-center w3-pale-green">
                            <input type='radio' name='jawaban5' value='3' required="required">
                        </td>
                    </tr>

                    <tr>
                        <td rowspan="2">6</td>
                        <td rowspan="2" style="width: 60%;">Saya merasa takut tanpa alasan yang jelas</td>
                        <td class="w3-center">
                            Tidak Ada
                        </td>
                        <td class="w3-center">
                            Kadang-kadang
                        </td>
                        <td class="w3-center">
                            Sering
                        </td>
                        <td class="w3-center">
                            Setiap saat
                        </td>
                    </tr>

                    <tr>
                        <td class="w3-center w3-pale-green">
                            <input type='radio' name='jawaban6' value='0' required="required">
                        </td>
                        <td class="w3-center w3-pale-green">
                            <input type='radio' name='jawaban6' value='1' required="required">
                        </td>
                        <td class="w3-center w3-pale-green">
                            <input type='radio' name='jawaban6' value='2' required="required">
                        </td>
                        <td class="w3-center w3-pale-green">
                            <input type='radio' name='jawaban6' value='3' required="required">
                        </td>
                    </tr>

                    <tr>
                        <td rowspan="2">7</td>
                        <td rowspan="2" style="width: 60%;">Saya cenderung bereaksi berlebihan terhadap situasi</td>
                        <td class="w3-center">
                            Tidak Ada
                        </td>
                        <td class="w3-center">
                            Kadang-kadang
                        </td>
                        <td class="w3-center">
                            Sering
                        </td>
                        <td class="w3-center">
                            Setiap saat
                        </td>
                    </tr>

                    <tr>
                        <td class="w3-center w3-pale-green">
                            <input type='radio' name='jawaban7' value='0' required="required">
                        </td>
                        <td class="w3-center w3-pale-green">
                            <input type='radio' name='jawaban7' value='1' required="required">
                        </td>
                        <td class="w3-center w3-pale-green">
                            <input type='radio' name='jawaban7' value='2' required="required">
                        </td>
                        <td class="w3-center w3-pale-green">
                            <input type='radio' name='jawaban7' value='3' required="required">
                        </td>
                    </tr>

                    <tr>
                        <td rowspan="2">8</td>
                        <td rowspan="2" style="width: 60%;">Saya khawatir tentang situasi di mana saya mungkin membodohi diri sendiri</td>
                        <td class="w3-center">
                            Tidak Ada
                        </td>
                        <td class="w3-center">
                            Kadang-kadang
                        </td>
                        <td class="w3-center">
                            Sering
                        </td>
                        <td class="w3-center">
                            Setiap saat
                        </td>
                    </tr>

                    <tr>
                        <td class="w3-center w3-pale-green">
                            <input type='radio' name='jawaban8' value='0' required="required">
                        </td>
                        <td class="w3-center w3-pale-green">
                            <input type='radio' name='jawaban8' value='1' required="required">
                        </td>
                        <td class="w3-center w3-pale-green">
                            <input type='radio' name='jawaban8' value='2' required="required">
                        </td>
                        <td class="w3-center w3-pale-green">
                            <input type='radio' name='jawaban8' value='3' required="required">
                        </td>
                    </tr>

                    <tr>
                        <td rowspan="2">9</td>
                        <td rowspan="2" style="width: 60%;">Saya merasa sulit untuk bersantai</td>
                        <td class="w3-center">
                            Tidak Ada
                        </td>
                        <td class="w3-center">
                            Kadang-kadang
                        </td>
                        <td class="w3-center">
                            Sering
                        </td>
                        <td class="w3-center">
                            Setiap saat
                        </td>
                    </tr>

                    <tr>
                        <td class="w3-center w3-pale-green">
                            <input type='radio' name='jawaban9' value='0' required="required">
                        </td>
                        <td class="w3-center w3-pale-green">
                            <input type='radio' name='jawaban9' value='1' required="required">
                        </td>
                        <td class="w3-center w3-pale-green">
                            <input type='radio' name='jawaban9' value='2' required="required">
                        </td>
                        <td class="w3-center w3-pale-green">
                            <input type='radio' name='jawaban9' value='3' required="required">
                        </td>
                    </tr>

                    <tr>
                        <td rowspan="2">10</td>
                        <td rowspan="2" style="width: 60%;">Sepertinya saya tidak merasakan perasaan positif sama sekali.</td>
                        <td class="w3-center">
                            Tidak Ada
                        </td>
                        <td class="w3-center">
                            Kadang-kadang
                        </td>
                        <td class="w3-center">
                            Sering
                        </td>
                        <td class="w3-center">
                            Setiap saat
                        </td>
                    </tr>

                    <tr>
                        <td class="w3-center w3-pale-green">
                            <input type='radio' name='jawaban10' value='0' required="required">
                        </td>
                        <td class="w3-center w3-pale-green">
                            <input type='radio' name='jawaban10' value='1' required="required">
                        </td>
                        <td class="w3-center w3-pale-green">
                            <input type='radio' name='jawaban10' value='2' required="required">
                        </td>
                        <td class="w3-center w3-pale-green">
                            <input type='radio' name='jawaban10' value='3' required="required">
                        </td>
                    </tr>

                    

                </table>

            </section>

          <div class="form-group">
            <label for="umur">Umur :</label>
            <select name="umur" id="umur" class="form-control selBox" type="radio" required="required">
                     
            </select>
          </div>

          <div class="form-group">
            <label for="umur">Tinggi Badan :</label>
            <select name="tinggi" id="tinggi" class="form-control selBox" required="required">
                <option value="" disabled selected>-- pilih tinggi--</option>
                <option value="kt">Kurang Tinggi ( < 150 cm - 159 cm )</option>
                <option value="ideal">Ideal ( 160 cm - 167 cm )</option>
                <option value="st">Sangat Tinggi ( >167 cm )</option>
            </select>
          </div>

          <div class="form-group">
            <label for="umur">Berat Badan :</label>
            <select name="beratB" id="beratB" class="form-control selBox" required="required">
                      <option value="" disabled selected>-- pilih berat badan--</option>
                      <option value="kurus">Kurus ( < 50 kg - 55 kg )</option>
                      <option value="ideal">Ideal ( 56 kg - 60 kg )</option>
                      <option value="tambun">Tambun ( >61 kg )</option>
                  </select>
          </div>

          <div class="form-group">
            <label for="umur">ancs :</label>
            <select name="kesehatan" id="kesehatan" class="form-control selBox" required="required">
                      <option value="" disabled selected>-- pilih status kesehatan--</option>
                      <option value="sehat">Sehat</option>
                      <option value="tidak_sehat">Tidak Sehat</option>
                  </select>
          </div>

          <div class="form-group">
            <label for="umur">Pendidikan :</label>
            <select name="pendidikan" id="pendidikan" class="form-control selBox" required="required">
                      <option value="" disabled selected>-- pilih pendidikan--</option>
                      <option value="sma">SMA</option>
                      <option value="smk">SMK</option>
                      <option value="s1">S1</option>
                  </select>
          </div>

         
          <div class="col">
          <div class="form-group">
            <input type="submit" value="Submit" class="btn btn-primary mt-3" id="dor" onclick="return simulasi()"/>
            <input type="reset" value="Reset Form" class="btn btn-primary mt-3" id="dor"/>
          </div>

          </form>
      </div>
    </div>
        
    <div class="col">
      <div class="col-20 mt-2 mb-3">
          <div id="hasilSIM" style="margin-bottom:30px;">
          </div>
      </div>
    </div>

    </div>

<!-- Footer -->
<footer class="page-footer font-small abu1 mt-5">

  <!-- Footer Elements -->
  <div class="container">

    <!-- Grid row-->
    <div class="row">

      <!-- Grid column -->
      <div class="col-md-12 py-5">

        <!-- <div class="text-center">
          Dibuat dengan <i class="fa fa-heart" style="color:#dc3545"></i> di Padang
        </div> -->
      </div>
      <!-- Grid column -->

    </div>
    <!-- Grid row-->

  </div>
  <!-- Footer Elements -->

  <!-- Copyright -->
  <div class="footer-copyright text-center py-3 abu2">©<?php echo date('Y'); ?> <a href="http://www.mycoding.net">Naïve Bayes Classifier</a>
  </div>
  <!-- Copyright -->

</footer>
<!-- Footer -->


<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="js/jquery.js"></script>
<script src="jspopper.min.js"></script>
<script src="js/bootstrap.min.js"></script>

<!-- validasi -->
<script>
  $(document).ready(function(){
    $('.toggle').click(function(){
      $('ul').toggleClass('active');
    });
  });
</script>

<script>
  function simulasi()
  {
    var umur = $("#umur").val();
    var tinggi_badan = $("#tinggi").val();
    var berat_badan = $("#beratB").val();
    var status_kesehatan = $("#kesehatan").val();
    var pendidikan = $("#pendidikan").val();

    //validasi
    var um = document.getElementById("umur");
    var tb = document.getElementById("tinggi");
    var bb = document.getElementById("beratB");
    var sk = document.getElementById("kesehatan");
    var pp = document.getElementById("pendidikan");

    if(um.selectedIndex == 0){
      alert("Umur Tidak Boleh Kosong");
      return false;
    }

    if(tb.selectedIndex == 0){
      alert("Tinggi Badan Tidak Boleh Kosong");
      return false;
    }

    if(bb.selectedIndex == 0){
      alert("Berat Badan Tidak Boleh Kosong");
      return false;
    }

    if(sk.selectedIndex == 0){
      alert("Status Kesehatan Tidak Boleh Kosong");
      return false;
    }

    if(pp.selectedIndex == 0){
      alert("Pendidikan Tidak Boleh Kosong");
      return false;
    }

    //batas validasi

      $.ajax({
        url :'simulasi.php',
        type : 'POST',
        dataType : 'html',
        data : {umur : umur , tinggi_badan : tinggi_badan , berat_badan : berat_badan , status_kesehatan : status_kesehatan , pendidikan : pendidikan},
        success : function(data){
          document.getElementById("hasilSIM").innerHTML = data;
        },
      });

      return false;

  }
</script>

<script>
$(document).ready(function(){
  $('#dor').click(function(){
    $('html, body').animate({
        scrollTop: $("#hasilSIM").offset().top
    }, 500);
  });
});
</script>
</body>
</html>
