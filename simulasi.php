<?php
require_once 'autoload.php';

$obj = new Bayes();

$jumTrue = $obj->sumTrue();
$jumFalse = $obj->sumFalse();
$jumData = $obj->sumData();

$a1 = $_POST['umur'];
$a2 = $_POST['tinggi_badan'];
$a3 = $_POST['berat_badan'];
$a4 = $_POST['status_kesehatan'];
$a5 = $_POST['pendidikan'];

//TRUE
$umur = $obj->probUmur($a1,1);
$tinggi = $obj->probTinggi($a2,1);
$bb = $obj->probBeratB($a3,1);
$kesehatan = $obj->probKesehatan($a4,1);
$pendidikan = $obj->probPendidikan($a5,1);

//FALSE
$umur2 = $obj->probUmur($a1,0);
$tinggi2 = $obj->probTinggi($a2,0);
$bb2 = $obj->probBeratB($a3,0);
$kesehatan2 = $obj->probKesehatan($a4,0);
$pendidikan2 = $obj->probPendidikan($a5,0);

//result
$paT = $obj->hasilTrue($jumTrue,$jumData,$umur,$tinggi,$bb,$kesehatan,$pendidikan);
$paF = $obj->hasilFalse($jumTrue,$jumData,$umur2,$tinggi2,$bb2,$kesehatan2,$pendidikan2);

if($a2 == "kt"){
  $a2 = "Kurang Tinggi";
}else if($a2 == "st"){
  $a2 = "Sangat Tinggi";
}

echo "<br>
<table class='table-bordered' style='font-size:20px;text-align:center' width='550px'>
  <tr style='background-color:#f5f5DC;color:#000000'>
    <td>Jumlah True</td><td>$jumTrue</td></tr> 
    <td>Jumlah False</td><td>$jumFalse</td></tr>
    <td>Jumlah Total Data</td><td>$jumData</td></tr>
</table>
";

echo "<br>
<table class='table-bordered' style='font-size:20px;text-align:center' width='550px'>
  <tr style='background-color:#f5f5DC;color:#000000'>
    <th></th>
    <th>True</th>
    <th>False</th>
  </tr>
  <tr>
    <td>pA</td>
    <td>$jumTrue / $jumData</td>
    <td>$jumFalse / $jumData</td>
  </tr>
  <tr>
    <td>Umur</td>
    <td>$umur / $jumTrue</td>
    <td>$umur2 / $jumFalse</td>
  </tr>
  <tr>
    <td>Tinggi Badan</td>
    <td>$tinggi / $jumTrue</td>
    <td>$tinggi2 / $jumFalse</td>
  </tr>
  <tr>
    <td>Berat Badan</td>
    <td>$bb / $jumTrue</td>
    <td>$bb2 / $jumFalse</td>
  </tr>
  <tr>
    <td>Status Kesehatan</td>
    <td>$kesehatan / $jumTrue</td>
    <td>$kesehatan2 / $jumFalse</td>
  </tr>
  <tr>
    <td>Pendidikan</td>
    <td>$pendidikan / $jumTrue</td>
    <td>$pendidikan2 / $jumFalse</td>
  </tr>
</table>
";

echo "<br>
<br>
<br>
<div class='col'>
  <table class='table-bordered' style='font-size:18px;text-align:center;'width='600px'>
    <tr style='background-color:#f5f5DC;color:#000000'>
      <th>PREDIKSI Diterima</th><td>$paT</td></tr>
      <th>PREDIKSI Ditolak</th><td>$paF</td></tr>  
    </tr>  
  </table>
";

$result = $obj->perbandingan($paT,$paF);

if($paT > $paF){
  echo "<br>
  <h3 class='tebal'>PREDIKSI <span class='badge badge-success' style='padding:10px'><b>DITERIMA</b></span> LEBIH BESAR DARI PADA PREDIKSI DITOLAK</h3><br>";
  echo "<h4><br>PREDIKSI diterima sebesar : <b>".round($result[1],2)." %</b> <br>PREDIKSI ditolak sebesar : <b>".round($result[2],2)." % </b></h4>";
}else if($paF > $paT){
  echo "<br>
  <h3 class='tebal'>PREDIKSI <span class='badge badge-danger' style='padding:10px'><b>DITOLAK</b></span> LEBIH BESAR DARI PADA PREDIKSI DITERIMA</h3><br>";
  echo "<h4><br>PREDIKSI ditolak sebesar : <b>".round($result[1],2)." %</b> <br>PREDIKSI diterima sebesar : <b>".round($result[2],2)." % </b></h4>";
}


if($result[0] == "DITERIMA"){
  echo "
  <div class='alert alert-success mt-5' role='aler'>
    <h4 class='alert-heading'>Kesimpulan : $result[0] </h4>
    <p>Selamat! berdasarkan hasil peritungan Naive Bayes, anda diprediksi akan <b>diterima!</b></p>
    <hr>
    <p class='mb-0'>- Have a nice day -</p>
  </div>";
}else{
  echo"
  <div class='alert alert-danger mt-5' role='aler'>
  <h4 class='alert-heading'>Kesimpulan : $result[0] </h4>
  <p>Maaf, berdasarkan hasil peritungan Naive Bayes, anda diprediksi akan <b>ditolak!</p>
  <hr>
  <p class='mb-0'>- Don't give up ! -</p>
  </div>";
}


 ?>
